# Projet web sémantique

## Sujet : Emission de dioxyde de carbonne dans le monde entre 2008 et 2018 

Ce projet à pour but de nous initier à la web sémantique. Nous avons donc pris des mesures de CO2 pour les traités et construire un dataset utilisable pour des applications concrètes comme des statistiques ou des études scientifique. 

## ETAPE 1 : Choix des données et transformation en RDF

- Construction des données grâce au .csv et au .sparql 
- Résultat dans le CO2.ttl

# ETAPE 2 : Liaison des datasets

- Les requetes dans les 2 dossiers sont à éxecuter en Java
- Utilisation de la librairie Jena nécessaire

# ETAPE 3 : Utilisation des ontologies OWL et inférences

- Construction de notre graphe avec des ontologies OWL présente dans le dosssier ontologie_protege
- Protégé à été utilisé pour construire des classes, des property et des individuals
- Nous avons pu déduire des inférances grâce à protégé

# ETAPE 4 : Liaison avec le linked data cloud

- Proposition de lien avec le linked data cloud sur le diaporama disponible ici : [diaporama](https://docs.google.com/presentation/d/1KCCkmKdwtJsprE8W8pk9NGQzP8Y0PIt5f3HQznuxId8/edit#slide=id.g6bd718f246_0_0)

# ETAPE 5 : Utilisation du vocabulaire VOID

- VOID disponible sans et avec notre dataset de base
- génération disponible en Java avec Jena