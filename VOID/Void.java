import org.apache.jena.rdf.model.Model;
import org.apache.jena.util.FileManager;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;

public class Void {
	public static void main (String args[]) {	    
		Model model = FileManager.get().loadModel("CO2.ttl");
		Model modelbis = FileManager.get().loadModel("VOID.ttl");

		Model THEmodel = modelbis.union(model);
		THEmodel.write(System.out,"Turtle");
		
		try(OutputStream out = new FileOutputStream("CO2_avec_VOID.ttl")) {
	        RDFDataMgr.write(out, THEmodel, RDFFormat.TURTLE_BLOCKS) ;
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
		System.out.println("done");
	}
}
