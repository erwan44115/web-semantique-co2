import org.apache.jena.graph.NodeFactory;
import org.apache.jena.graph.Triple;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.util.FileManager;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;

public class CO2_mortality {
	public static void main (String args[]) {	    
		Model model = FileManager.get().loadModel("CO2.ttl");
		Model modelbis = FileManager.get().loadModel("Global_mortality.ttl");
	
		org.apache.jena.graph.Node year = NodeFactory.createURI("http://dbpedia.org/ontology/year");
		org.apache.jena.graph.Node predicate = NodeFactory.createURI("http://www.w3.org/2002/07/owl#sameAs");
		org.apache.jena.graph.Node year2 = NodeFactory.createURI("http://dbpedia.org/page/Year");
		Triple triple = Triple.create(year, predicate, year2);
		modelbis.add(modelbis.asStatement(triple));
		
		Model THEmodel = modelbis.union(model);
		THEmodel.write(System.out,"Turtle");
		
		try(OutputStream out = new FileOutputStream("CO2_mortality.ttl")) {
	        RDFDataMgr.write(out, THEmodel, RDFFormat.TURTLE_BLOCKS) ;
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
		System.out.println("done");
		
		String querytruc =
				"PREFIX dbpedia: <http://fr.dbpedia.org/page/>\n"+
				"PREFIX owl: <http://www.w3.org/2002/07/owl#>\n"+
				"PREFIX db: <http://dbpedia.org/ontology/>\n"+
				"SELECT DISTINCT ?region ?year ?tauxCO2 ?Respiratory_tract_infection ?Lower_respiratory_infections\n"+
				"WHERE {"+
						"{?x dbpedia:Respiratory_tract_infection ?Respiratory_tract_infection;"+
						"dbpedia:Lower_respiratory_infections ?Lower_respiratory_infections;"+
						"dbpedia:Year ?year;"+
						"owl:sameAs ?region."+
						"} UNION {"+
						"?y db:year ?year;"+
						"db:location ?region;"+
						"db:value ?tauxCO2}"+
						"FILTER (((?year = '2015') && (regex(?Respiratory_tract_infection,'^[1-9]+[0-9]+'))) || ((?year = 2015) && (?tauxCO2>10000)))"+
				"}\n"+
				"ORDER BY DESC (?Respiratory_tract_infection)";
		
		Query query=QueryFactory.create(querytruc);
		QueryExecution qe = QueryExecutionFactory.create(query, THEmodel);
	    ResultSet results =  qe.execSelect();
	    ResultSetFormatter.out(System.out, results, query);
	    qe.close();
	}
}