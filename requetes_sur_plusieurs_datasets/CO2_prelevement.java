import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.graph.Triple;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.util.FileManager;
import org.apache.log4j.BasicConfigurator;

public class CO2_prelevement {
	public static void main (String args[]) {	    
		BasicConfigurator.configure();
		Model model = FileManager.get().loadModel("CO2.ttl");
		Model modelbis = FileManager.get().loadModel("rdfPrelevement.ttl");
	
		org.apache.jena.graph.Node year = NodeFactory.createURI("http://dbpedia.org/ontology/year");
		org.apache.jena.graph.Node predicate = NodeFactory.createURI("http://www.w3.org/2002/07/owl#sameAs");
		org.apache.jena.graph.Node year2 = NodeFactory.createURI("http://ns.nature.com/terms/date");
		Triple triple = Triple.create(year, predicate, year2);
		modelbis.add(modelbis.asStatement(triple));
		
		Model THEmodel = modelbis.union(model);
		THEmodel.write(System.out,"Turtle");
		
		try(OutputStream out = new FileOutputStream("CO2_prelevement.ttl")) {
	        RDFDataMgr.write(out, THEmodel, RDFFormat.TURTLE_BLOCKS) ;
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
		System.out.println("done");
		
		String querytruc =
				"PREFIX db: <http://dbpedia.org/ontology/>\n"+
				"PREFIX npg: <http://ns.nature.com/terms/>\n"+
				"PREFIX w3id: <https://w3id.org/seas/>\n"+
				"PREFIX dbo:  <http://dbpedia.org/ontology/> \n"+
				"PREFIX fn: <http://www.w3.org/2005/xpath-functions#>"+
				"SELECT DISTINCT ?year ?regionFrance ?tauxCompany ?regionMonde (((?taux*1000000)*1000) AS ?tauxRegion) (fn:round(((?tauxCompany*100)/?tauxRegion),8) AS ?pourcentage) \n"+
				"WHERE {"+
				    "?x w3id:value ?tauxCompany;"+
				    	"npg:date ?year;"+
				    	"dbo:Company <http://dbpedia.org/ontology/Company/067.00470>."+
				    "<http://dbpedia.org/ontology/Company/067.00470> dbo:region ?regionFrance."+
				    "OPTIONAL {"+
				    "?y db:year ?year;"+
				    "db:location ?regionMonde;"+
				    "db:value ?taux}"+
				"}\n"+
				"ORDER BY DESC (?year)";
		
		Query query=QueryFactory.create(querytruc);
		QueryExecution qe = QueryExecutionFactory.create(query, THEmodel);
	    ResultSet results =  qe.execSelect();
	    ResultSetFormatter.out(System.out, results, query);
	    qe.close();
	}
}
