import org.apache.jena.graph.NodeFactory;
import org.apache.jena.graph.Triple;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.util.FileManager;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;

public class CO2_mortality_prelevement {
	public static void main (String args[]) {	    
		Model model = FileManager.get().loadModel("CO2.ttl");
		Model modelbis = FileManager.get().loadModel("Global_mortality.ttl");
		Model modelbisbis = FileManager.get().loadModel("rdfPrelevement.ttl");
	
		org.apache.jena.graph.Node year = NodeFactory.createURI("http://dbpedia.org/ontology/year");
		org.apache.jena.graph.Node predicate = NodeFactory.createURI("http://www.w3.org/2002/07/owl#sameAs");
		org.apache.jena.graph.Node year2 = NodeFactory.createURI("http://dbpedia.org/page/Year");
		Triple triple = Triple.create(year, predicate, year2);
		modelbis.add(modelbis.asStatement(triple));

		org.apache.jena.graph.Node year3 = NodeFactory.createURI("http://ns.nature.com/terms/date");
		triple = Triple.create(year, predicate, year3);
		modelbisbis.add(modelbisbis.asStatement(triple));
		
		Model THEmodel = modelbis.union(model);
		THEmodel = THEmodel.union(modelbisbis);
		THEmodel.write(System.out,"Turtle");
		
		try(OutputStream out = new FileOutputStream("CO2_mortality_prelevement.ttl")) {
	        RDFDataMgr.write(out, THEmodel, RDFFormat.TURTLE_BLOCKS) ;
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
		System.out.println("done");
		
		String querytruc =
				"PREFIX dbpedia: <http://fr.dbpedia.org/page/>\n"+
				"PREFIX owl: <http://www.w3.org/2002/07/owl#>\n"+
				"PREFIX db: <http://dbpedia.org/ontology/>\n"+
				"PREFIX npg: <http://ns.nature.com/terms/>\n"+
				"PREFIX w3id: <https://w3id.org/seas/>\n"+
				"PREFIX dbo:  <http://dbpedia.org/ontology/>\n"+
				"PREFIX fn: <http://www.w3.org/2005/xpath-functions#>"+
				"SELECT DISTINCT ?year (((?taux*1000000)*1000) AS ?tauxCO2enEurope) ?tauxCompany (fn:round(((?tauxCompany*100)/?tauxCO2enEurope),8) AS ?pourcentage) ?typeEmissionPolluante ?Respiratory_tract_infection_France ?Lower_respiratory_infections_France\n"+ 
				"WHERE {"+
						"{?x dbpedia:Respiratory_tract_infection ?Respiratory_tract_infection_France;"+
						"dbpedia:Lower_respiratory_infections ?Lower_respiratory_infections_France;"+
						"dbpedia:Year ?year;"+
						"owl:sameAs 'France'."+
						"} UNION {"+
						"?z w3id:value ?tauxCompany;" + 
						"npg:date ?year;" + 
						"dbo:Company <http://dbpedia.org/ontology/Company/067.00470>;" +
						"<http://spinrdf.org/sp#Sample> ?typeEmissionPolluante." +
						"<http://dbpedia.org/ontology/Company/067.00470> dbo:region ?regionFrance."+
						"} OPTIONAL {"+
						"?y db:year ?year;"+
						"db:location 'Europe';"+
						"db:value ?taux}"+
						"FILTER (?year = '2009' || ?year = 2009)"+
				"}\n"+
				"ORDER BY DESC (?Respiratory_tract_infection_France)";
		
		Query query=QueryFactory.create(querytruc);
		QueryExecution qe = QueryExecutionFactory.create(query, THEmodel);
	    ResultSet results =  qe.execSelect();
	    ResultSetFormatter.out(System.out, results, query);
	    qe.close();
	}
}
