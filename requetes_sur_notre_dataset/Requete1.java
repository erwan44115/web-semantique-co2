import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.util.FileManager;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;

public class Requete1 {
	public static void main (String args[]) {	    
		Model model = FileManager.get().loadModel("CO2.ttl");
		model.write(System.out,"Turtle");
		
		try(OutputStream out = new FileOutputStream("result.ttl")) {
	        RDFDataMgr.write(out, model, RDFFormat.TURTLE_BLOCKS) ;
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
		
		String querytruc =
				"PREFIX dbo: <http://dbpedia.org/ontology/>\n"+
				"SELECT DISTINCT ?region ?year ?value\n"+
				"WHERE {"+
						"?x dbo:year ?year;"+
						"dbo:location ?region;"+
						"dbo:value ?value."+
						"FILTER (?value >= 5000)"+
				"}\n"+
				"ORDER BY ASC (?year)";
		
		Query query=QueryFactory.create(querytruc);
		QueryExecution qe = QueryExecutionFactory.create(query, model);
	    ResultSet results =  qe.execSelect();
	    ResultSetFormatter.out(System.out, results, query);
	    qe.close();
	}
}
